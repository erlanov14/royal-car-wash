//
//  PriceViewController.m
//  Rolyal Car Wash
//
//  Created by Yerassyl on 15.09.15.
//  Copyright (c) 2015 acelight. All rights reserved.
//

#import "PriceViewController.h"
#import "UserObj.h"

@interface PriceViewController ()
@property (strong, nonatomic) IBOutlet UILabel *priceLabel;

@end

@implementation PriceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[UserObj sharedInstance] setFromSuccessPrice:0];
    _priceLabel.text = [[UserObj sharedInstance] curantPrice];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)goBackBtnTouched:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
