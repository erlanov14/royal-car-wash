//
//  ComplexesViewController.m
//  Rolyal Car Wash
//
//  Created by Yerassyl on 13.09.15.
//  Copyright (c) 2015 acelight. All rights reserved.
//

#import "ComplexesViewController.h"
#import "EnrollViewController.h"
#import "ComplexCell.h"
#import "WaitingView.h"
#import "AFViewShaker.h"
#import "RoyalModel.h"
#import "UserObj.h"
#import "Defs.h"
#import "AppDelegate.h"

#define kNumberOfCellsInARow 2


@interface ComplexesViewController ()<UICollectionViewDataSource,UICollectionViewDelegate,UIActionSheetDelegate>
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UIImageView *asTypeImgVIew;
@property (strong, nonatomic) IBOutlet UILabel *complexChooseLabel;
@property (strong, nonatomic) IBOutlet UILabel *priceLabel;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) IBOutlet UIButton *nextBtn;
@property (strong, nonatomic) IBOutlet UILabel *onLineLabel;

@end

@implementation ComplexesViewController {
    NSArray *complexes;
    int isChosen[10], sumPrice;
    
    RoyalModel *rModel;
    WaitingView *wView;
    
    NSDictionary *aService;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    rModel = [RoyalModel new];
    wView = [WaitingView new];
    sumPrice = 0;
    
    for(int i=0;i<10;i++)isChosen[i] = 0;
    
    
    [self initCollection];
    [self updateCollectionViewLayout];
    [self customize];
    [self getAutoService];
    
    _onLineLabel.text = [NSString stringWithFormat:@"%d",[[UserObj sharedInstance] ordersCount]];
    _nameLabel.text = str([[UserObj sharedInstance] info][@"name"]);
}

- (void) getAutoService {
    if ( ![APP hasInternet] ) {
        [[[UIAlertView alloc] initWithTitle:@"Внимание" message:@"Проверьте подключение к интернету" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
        return;
    }
    
    [wView addWaitinViewToView:self.view];
    [rModel getAutoServiceWithASID:_asID completion:^(BOOL success, NSDictionary *autoServic, NSString *error) {
        if ( success ) {
            aService = autoServic;
            complexes = @[
                          @{@"keyT":@"salon", @"title":@"Салон", @"img":@"salon.png", @"imgA":@"salon_active.png", @"price":str([aService objectForKey:@"salon"])},
                          @{@"keyT":@"bagazhnik", @"title":@"Багажник", @"img":@"baggage.png", @"imgA":@"baggage_active.png", @"price":str([aService objectForKey:@"bagazhnik"])},
                          @{@"keyT":@"kuzov", @"title":@"Кузов", @"img":@"body.png", @"imgA":@"body_active.png", @"price":str([aService objectForKey:@"kuzov"])},
                          @{@"keyT":@"himchistka", @"title":@"Химчистка", @"img":@"dry_cleaning.png", @"imgA":@"dry_cleaning_active.png", @"price":str([aService objectForKey:@"himchistka"])},
                          @{@"keyT":@"motor", @"title":@"Мотор", @"img":@"motor.png", @"imgA":@"motor_active.png", @"price":str([aService objectForKey:@"motor"])},
                          @{@"keyT":@"nano", @"title":@"Нано покрытие", @"img":@"nano.png", @"imgA":@"nano_active.png", @"price":str([aService objectForKey:@"nano"])},
                          @{@"keyT":@"bez_moika", @"title":@"Безконтактная мойика", @"img":@"noncontact.png", @"imgA":@"noncontact_active.png", @"price":str([aService objectForKey:@"bez_moika"])},
                          @{@"keyT":@"oblivka", @"title":@"Обливка", @"img":@"oblivka.png", @"imgA":@"oblivka_active.png", @"price":str([aService objectForKey:@"oblivka"])},
                          @{@"keyT":@"polirovka", @"title":@"Полировка", @"img":@"polishing.png", @"imgA":@"polishing_active.png", @"price":str([aService objectForKey:@"polirovka"])}
                            ];
            [_collectionView reloadData];
        }
        else {
            [[[UIAlertView alloc] initWithTitle:@"Внимание" message:error delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
        }
        [wView removeWaitActivity];
    }];
}



- (void) customize {
    _nextBtn.layer.cornerRadius = 5;
    _asTypeImgVIew.image = [UIImage imageNamed:_asImg];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)goBackBtnTouched:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)nextBtnTouched:(id)sender {
    int chosenInd = -1;
    NSString *serviceTypes=@"";
    
    for(int i=0;i<9;i++){
        if ( isChosen[i] == 1 ) {
            chosenInd = i;
            if([serviceTypes isEqualToString:@""]) serviceTypes = str(complexes[i][@"keyT"]);
            else serviceTypes = [NSString stringWithFormat:@"%@,%@",serviceTypes,str(complexes[i][@"keyT"])];
        }
    }
    if ( chosenInd == -1 ) {
        [[[AFViewShaker alloc] initWithView:_complexChooseLabel]  shakeWithDuration:0.6 completion:^{}];
        return;
    }
    
    
    if ( _isPricecurant == 1 ) {
        [[UserObj sharedInstance] setCurantPrice:_priceLabel.text];
        [[UserObj sharedInstance] setFromSuccessPrice:1];
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
    else {
        EnrollViewController *cvc = [self.storyboard instantiateViewControllerWithIdentifier:@"EnrollViewController"];
        cvc.sumPrice = sumPrice;
        cvc.asID = _asID;
        cvc.serviceTypes = serviceTypes;
        
        [self.navigationController pushViewController:cvc animated:YES];
    }
}

- (IBAction)menuBtnTouched:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:@"Отмена"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Выйти из аккаунта", nil];
    [actionSheet showInView:self.view];
}
#pragma mark - Action sheet delegate
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ( buttonIndex == 0 ) {
        [[UserObj sharedInstance] setInfo:[NSDictionary new]];
        //[[UserObj sharedInstance] setCurrentOrder:nil];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

#pragma mark - collection view delegate
- (void)initCollection {
    ((UIScrollView*)(_collectionView)).delegate = self;
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    
    
    _collectionView.backgroundColor = [UIColor clearColor];
    self.collectionView.alwaysBounceVertical = YES;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return complexes.count;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 4.0;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 8.0;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ComplexCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ComplexCell" forIndexPath:indexPath];
    if ( isChosen[indexPath.row] == 0 ) [cell initWithTitle:[complexes[indexPath.row] objectForKey:@"title"] price:[complexes[indexPath.row] objectForKey:@"price"] img:[complexes[indexPath.row] objectForKey:@"img"]];
    else [cell initWithTitle:[complexes[indexPath.row] objectForKey:@"title"] price:[complexes[indexPath.row] objectForKey:@"price"] img:[complexes[indexPath.row] objectForKey:@"imgA"]];
    
    if ( isChosen[indexPath.row] == 1 ) {
        cell.backgroundColor = RGBA(32, 174, 245, 0.5);
        cell.checkBoxImgView.image = [UIImage imageNamed:@"service_checkbox_active.png"];
    }
    else {
        cell.backgroundColor = RGBA(255, 255, 255, 0.08);
        cell.checkBoxImgView.image = [UIImage imageNamed:@"service_checkbox.png"];
    }
    
    return cell;
}
- (void)updateCollectionViewLayout
{
    UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout;
    CGFloat size = (Scn_Width-22) / kNumberOfCellsInARow;
    
    if ( IS_IPHONE_6P ) size -= 3;
    //NSLog(@"%f",size);
    layout.itemSize = CGSizeMake(size, size*0.647);
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath  {
    ComplexCell *datasetCell = (ComplexCell*)[collectionView cellForItemAtIndexPath:indexPath];
    if ( isChosen[indexPath.row] == 0 ) {
        datasetCell.imgView.image = [UIImage imageNamed:[complexes[indexPath.row] objectForKey:@"imgA"]];
        datasetCell.backgroundColor = RGBA(32, 174, 245, 0.5);
        datasetCell.checkBoxImgView.image = [UIImage imageNamed:@"service_checkbox_active.png"];
        isChosen[indexPath.row] = 1;
        sumPrice += [[complexes[indexPath.row] objectForKey:@"price"] intValue];
    }
    else {
        datasetCell.backgroundColor = RGBA(255, 255, 255, 0.08);
        datasetCell.imgView.image = [UIImage imageNamed:[complexes[indexPath.row] objectForKey:@"img"]];
        datasetCell.checkBoxImgView.image = [UIImage imageNamed:@"service_checkbox.png"];
        isChosen[indexPath.row] = 0;
        sumPrice -= [[complexes[indexPath.row] objectForKey:@"price"] intValue];
    }
    _priceLabel.text = strFormat(@"%d тг", sumPrice);
}




@end
