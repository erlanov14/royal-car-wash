//
//  LoginViewController.m
//  Rolyal Car Wash
//
//  Created by Yerassyl on 11.09.15.
//  Copyright (c) 2015 acelight. All rights reserved.
//

#import "LoginViewController.h"
#import "MainViewController.h"
#import "AFViewShaker.h"
#import "RoyalModel.h"
#import "WaitingView.h"
#import "Defs.h"
#import "AppDelegate.h"

@interface LoginViewController () <UIPickerViewDataSource,UIPickerViewDelegate>
@property (strong, nonatomic) IBOutlet UITextField *nameTextField;
@property (strong, nonatomic) IBOutlet UITextField *phoneTxtField;
@property (strong, nonatomic) IBOutlet UITextField *carNumberTxtField;
@property (strong, nonatomic) IBOutlet UIPickerView *pickerView1;
@property (strong, nonatomic) IBOutlet UIPickerView *pickerView2;
@property (strong, nonatomic) IBOutlet UIButton *fizBtn;
@property (strong, nonatomic) IBOutlet UIButton *urBtn;
@property (strong, nonatomic) IBOutlet UIButton *okBtn;



@end

@implementation LoginViewController {
    NSMutableArray *pickerViewDataSource1;
    RoyalModel *rModel;
    WaitingView *wView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    rModel = [RoyalModel new];
    wView = [WaitingView new];
    
    pickerViewDataSource1 = [NSMutableArray arrayWithArray:@[@"KZ",@"RU"]];
    [self customize];
}


- (void) customize {
    _okBtn.layer.cornerRadius = 5;
    
    NSArray *ar = @[_nameTextField, _phoneTxtField];
    NSArray *imgs = @[@"white_person.png",@"white_phone.png"];
    int i=0;
    for (UITextField *txtF in ar) {
        UIView *myV = [[UIView alloc] initWithFrame:CGRectMake(0, txtF.frame.size.height-1, txtF.frame.size.width, 1)];
        myV.backgroundColor = RGBA(224, 224, 224, 1);
        [txtF addSubview:myV];
        
        UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, txtF.frame.size.height)];
        txtF.leftView = paddingView;
        txtF.leftViewMode = UITextFieldViewModeAlways;
        NSAttributedString *placeholder = [[NSAttributedString alloc] initWithString:txtF.placeholder attributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor] }];
        txtF.attributedPlaceholder = placeholder;
        
        UIImageView *myI = [[UIImageView alloc] initWithFrame:CGRectMake(3, 0, txtF.frame.size.height/2, txtF.frame.size.height-8)];
        myI.image = [UIImage imageNamed:imgs[i]];
        [myI setContentMode:UIViewContentModeScaleAspectFit];
        [txtF addSubview:myI];
        i++;
    }
    NSAttributedString *placeholder = [[NSAttributedString alloc] initWithString:_carNumberTxtField.placeholder attributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor] }];
    _carNumberTxtField.attributedPlaceholder = placeholder;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)okBtnTouched:(id)sender {
    if ( ![APP hasInternet] ) {
        [[[UIAlertView alloc] initWithTitle:@"Внимание" message:@"Проверьте подключение к интернету" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
        return;
    }
    
    NSArray *arr = @[_nameTextField, _phoneTxtField, _carNumberTxtField];
    int isOk = 1;
    for (UITextField *txtF in arr) {
        [txtF resignFirstResponder];
        if ([txtF.text isEqual:@""]){
            isOk = 0;
            [[[AFViewShaker alloc] initWithView:txtF]  shakeWithDuration:0.6 completion:^{}];
        }
    }
    if (isOk == 0) return;
    NSString *sts = @"1";
    if (_urBtn.tag == 1) sts = @"2";
    [wView addWaitinViewToView:self.view];
    [rModel registerUserWithName:_nameTextField.text phone:_phoneTxtField.text carNumber:_carNumberTxtField.text status:sts completion:^(BOOL success, NSString *error) {
        if ( success ) {
            MainViewController *mvc = [self.storyboard instantiateViewControllerWithIdentifier:@"MainViewController"];
            UINavigationController *nvc = [[UINavigationController alloc] initWithRootViewController:mvc];
            [nvc setNavigationBarHidden:YES];
            [self presentViewController:nvc animated:YES completion:nil];
        }
        else {
            [[[UIAlertView alloc] initWithTitle:@"Внимание" message:error delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
        }
        [wView removeWaitActivity];
    }];

}
- (IBAction)urFizBtnTouched:(id)sender {
    UIButton *btn = (UIButton*)sender;
    if ( btn.tag == 1 ) return;
    _fizBtn.tag = 0;
    _urBtn.tag = 0;
    [_fizBtn setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
    [_urBtn setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
    
    btn.tag = 1;
    [btn setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
}
- (IBAction)goBackBtnTouched:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - PickerViewDelegate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}


- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if ( pickerView == _pickerView1 ) return pickerViewDataSource1.count;
    return 16;
}
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 35;
}
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    UILabel *pickerLabel = [UILabel new];
    pickerLabel.textColor = [UIColor whiteColor];
    pickerLabel.font = [UIFont fontWithName:@"Arial-BoldMT" size:15];
    pickerLabel.textAlignment = NSTextAlignmentCenter;
    
    if (pickerView == _pickerView1 ){
        pickerLabel.text = pickerViewDataSource1[row];
    }
    else {
        pickerLabel.text =  [NSString stringWithFormat:@"%02d", (int)row+1];
    }
    return pickerLabel;
}



@end
