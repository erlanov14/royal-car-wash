//
//  UserObj.m
//  Rolyal Car Wash
//
//  Created by Yerassyl on 11.09.15.
//  Copyright (c) 2015 acelight. All rights reserved.
//

#import "UserObj.h"

@implementation UserObj

+ (id)sharedInstance {
    static UserObj *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[[self class] alloc] init];
    });
    return sharedMyManager;
}
- (id)init {
    if (self = [super init]) {
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        _info = [prefs objectForKey:@"UserObjInfo"];
        _deviceToken=@"no_dev_token";
        _carWashes = nil;
    }
    return self;
}
- (NSDictionary *)getInfo {
    return _info;
}
- (void)setInfo:(NSDictionary *)info {
    _info = info;
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:info forKey:@"UserObjInfo"];
    [prefs synchronize];
}

@end
