//
//  LeftTimeViewController.m
//  Rolyal Car Wash
//
//  Created by Yerassyl on 11.09.15.
//  Copyright (c) 2015 acelight. All rights reserved.
//

#import "LeftTimeViewController.h"
#import "RoyalModel.h"
#import "WaitingView.h"
#import "UserObj.h"
#import "Defs.h"
#import "AppDelegate.h"

@interface LeftTimeViewController ()
@property (strong, nonatomic) IBOutlet UIButton *cancelOrderBtn;
@property (strong, nonatomic) IBOutlet UILabel *arrivalTimeLabel;
@property (strong, nonatomic) IBOutlet UILabel *leftTimeLabel;
@property (strong, nonatomic) IBOutlet UILabel *boxLabel;
@property (strong, nonatomic) IBOutlet UILabel *orderCodeLabel;

@end

@implementation LeftTimeViewController {
    int hour,minute,second;
 
    RoyalModel *rModel;
    WaitingView *wView;
    
    NSDictionary *currentOrder;
    NSTimer *myTimer;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    [[UserObj sharedInstance] setFromSuccessEnroll:0];
    
    rModel = [RoyalModel new];
    wView = [WaitingView new];

    
    [self customize];
    [self getLastOrders];
}


- (void) startTimer {
    _boxLabel.text = [NSString stringWithFormat:@"БОКС №%@",currentOrder[@"box_number"]];
    _orderCodeLabel.text = [NSString stringWithFormat:@"Код заказа: %@",currentOrder[@"order_number"]];
    
    NSDictionary *cOrder = currentOrder;
    int aa = [[cOrder objectForKey:@"date"] intValue];
    NSLog(@"timestamp form server %d", aa);
    
    NSDate *orederDate = [NSDate dateWithTimeIntervalSince1970:aa];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond) fromDate:orederDate];
    int ch = (int)[components hour], cm = (int)[components minute];
    _arrivalTimeLabel.text = [NSString stringWithFormat:@"%02d-%02d",ch,cm];
    
    
    int timeDifferernce = aa-[[NSDate date] timeIntervalSince1970];
    NSLog(@"difference %d", timeDifferernce);
    
    hour = timeDifferernce/3600;
    minute =  (timeDifferernce%3600)/60;
    second = timeDifferernce%60;
    
    if ( second < 0 ) second += 60, minute--;
    if ( minute < 0 ) minute += 60, hour--;
    if ( hour < 0 ) hour += 24;
    
    [self setLeftTime];
    myTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateTime:) userInfo:nil repeats:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    if ( myTimer ) [myTimer invalidate];
}

- (void) getLastOrders {
    if ( ![APP hasInternet] ) {
        [[[UIAlertView alloc] initWithTitle:@"Внимание" message:@"Проверьте подключение к интернету" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
        return;
    }
    
    [wView addWaitinViewToView:self.view];
    [rModel getLastOrderWithCompletion:^(BOOL success, NSDictionary *order, NSString *error) {
        if ( success ) {
            currentOrder = order;
            _cancelOrderBtn.hidden = NO;
            [self startTimer];
            //NSLog(@"ORDER %@",order);
        }
        else {
            _cancelOrderBtn.hidden = YES;
            [[[UIAlertView alloc] initWithTitle:@"Внимание" message:error delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
        }
        [wView removeWaitActivity];
    }];
}


- (void)setLeftTime {
    _leftTimeLabel.text = [NSString stringWithFormat:@"%02d-%02d-%02d",hour,minute,second];
}

- (void)updateTime:(NSTimer*)timer {
    if ( second == 0 && minute == 0 && hour == 0 ) {
        if ( myTimer ) [myTimer invalidate];
        [[[UIAlertView alloc] initWithTitle:@"Внимание" message:[NSString stringWithFormat:@"Вас ожидает БОКС №%@",currentOrder[@"box_number"]] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
        return;
    }
    second--;
    if ( second < 0 ) second += 60, minute--;
    if ( minute < 0 ) minute += 60, hour--;
    [self setLeftTime];
}

- (void) customize {
    _cancelOrderBtn.layer.cornerRadius = 5;
    _cancelOrderBtn.hidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)goBackBtnTouched:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)cancelOrderBtnTouched:(id)sender {
    if ( !currentOrder ) return;
    if ( ![APP hasInternet] ) {
        [[[UIAlertView alloc] initWithTitle:@"Внимание" message:@"Проверьте подключение к интернету" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
        return;
    }
    
    hour = 0, minute = 0, second = 1;
    _boxLabel.hidden = YES;
    _orderCodeLabel.hidden = YES;
    _arrivalTimeLabel.text = @"00-00";
    _cancelOrderBtn.hidden = YES;
    _leftTimeLabel.text = [NSString stringWithFormat:@"%02d-%02d-%02d",0,0,0];
    _boxLabel.text = @"0";
    
    if ( sender != nil ) {
        [wView addWaitinViewToView:self.view];
        [rModel destroyOrderWithBoxID:str(currentOrder[@"box_id"]) orderID:str(currentOrder[@"_id"][@"$oid"]) completion:^(BOOL success, NSString *error) {
            if ( success ) {
                [[[UIAlertView alloc] initWithTitle:@"Внимание" message:@"Вы отменили очередь" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
                if (myTimer) [myTimer invalidate];
            }
            else {
                [[[UIAlertView alloc] initWithTitle:@"Внимание" message:error delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
            }
            [wView removeWaitActivity];
        }];
    }
}

@end
