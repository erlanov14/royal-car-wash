//
//  RoyalModel.h
//  Rolyal Car Wash
//
//  Created by Yerassyl on 11.09.15.
//  Copyright (c) 2015 acelight. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RoyalModel : NSObject

- (void) registerUserWithName:(NSString*)name phone:(NSString*)phone carNumber:(NSString*)carNumber status:(NSString*)status completion:(void (^)(BOOL success, NSString *error))completionBlock;
- (void) authorizeWithPhone:(NSString*)phone completion:(void (^)(BOOL success, NSString *error))completionBlock;

- (void) getPromotionsWithCompletion:(void (^)(BOOL success,NSArray *promotions , NSString *error))completionBlock;


- (void) getAutoServicesWithCompletion:(void (^)(BOOL success,NSArray *autoServices , NSString *error))completionBlock;
- (void) getAutoServiceWithASID:(NSString*)asID completion:(void (^)(BOOL success,NSDictionary *autoService , NSString *error))completionBlock;

- (void) getFreeTimesInCarwashWithID:(NSString*)asID completion:(void (^)(BOOL success,NSDictionary *freeTimes , NSString *error))completionBlock;


- (void) getCarWashesWithCompletion:(void (^)(BOOL success,NSArray *carWashes , NSString *error))completionBlock;

- (void) doOrderWithCarwashID:(NSString*)carwashID time:(NSString*)time auto_tip:(NSString*)auto_tip service_types:(NSString*)service_types online_order:(NSString*)online_order completion:(void (^)(BOOL success,NSDictionary *order , NSString *error))completionBlock;
- (void) destroyOrderWithBoxID:(NSString*)boxID orderID:(NSString*)orderID completion:(void (^)(BOOL success, NSString *error))completionBlock;

- (void) getOrdersCount;

- (void) getLastOrderWithCompletion:(void (^)(BOOL success,NSDictionary *order, NSString *error))completionBlock;

- (void) sendDeviceToken;

@end
