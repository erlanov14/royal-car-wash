//
//  EnrollViewController.m
//  Rolyal Car Wash
//
//  Created by Yerassyl on 13.09.15.
//  Copyright (c) 2015 acelight. All rights reserved.
//

#import "EnrollViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "CategorySliderView.h"
#import "WaitingView.h"
#import "AFViewShaker.h"
#import "RoyalModel.h"
#import "UserObj.h"
#import "Defs.h"
#import "AppDelegate.h"
@import GoogleMaps;


#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)


@interface EnrollViewController () <UIActionSheetDelegate>
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *onLineLabel;
@property (strong, nonatomic) IBOutlet UILabel *priceLabel;
@property (strong, nonatomic) IBOutlet UIButton *rcwBtn;
@property (strong, nonatomic) IBOutlet UIButton *korkemBtn;
@property (strong, nonatomic) IBOutlet UIButton *enrollBtn;
@property (strong, nonatomic) IBOutlet UIView *viewForTimeChooser;
@property (strong, nonatomic) IBOutlet UIView *mapContainerView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *bottomOffsetConstraint;

@end

@implementation EnrollViewController {
    CategorySliderView *timeChooser1,*timeChooser2;
    
    NSArray *carWashes;
    NSMutableArray *freeTimesArray1,*freeTimesArray2;
    NSDictionary *freeTimes1, *freeTimes2;
    NSString *selectedTime1, *selectedTime2;
    
    RoyalModel *rModel;
    WaitingView *wView;
    

    CLLocationManager *locationManager;
    GMSMapView *mapView_;
    GMSMarker *marker;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    rModel = [RoyalModel new];
    wView = [WaitingView new];
    
    
    selectedTime1 = @"09:00";
    selectedTime2 = @"09:00";
    
    [self initGoogleMaps];
    [self customize];
    if ( ![[UserObj sharedInstance] carWashes] ) [self getCarWashes];
    else                                         carWashes = [[UserObj sharedInstance] carWashes],[self getFreeTimes];
    
    _onLineLabel.text = [NSString stringWithFormat:@"%d",[[UserObj sharedInstance] ordersCount]];
    _nameLabel.text = str([[UserObj sharedInstance] info][@"name"]);
}
- (void)viewDidAppear:(BOOL)animated {
    mapView_.frame = CGRectMake(0, 0, _mapContainerView.frame.size.width, _mapContainerView.frame.size.height);
}
- (void)initGoogleMaps {
    locationManager = [[CLLocationManager alloc] init];
#ifdef IS_OS_8_OR_LATER
    if(IS_OS_8_OR_LATER) {
        [locationManager requestWhenInUseAuthorization];
        [locationManager requestAlwaysAuthorization];
    }
#endif
    [locationManager startUpdatingLocation];
    
    mapView_ = [GMSMapView mapWithFrame:CGRectZero camera:nil];
    mapView_.myLocationEnabled = YES;
    mapView_.frame = CGRectMake(0, 0, _mapContainerView.frame.size.width, _mapContainerView.frame.size.height);
    [_mapContainerView addSubview:mapView_];
}


- (void) getCarWashes {
    if ( ![APP hasInternet] ) {
        [[[UIAlertView alloc] initWithTitle:@"Внимание" message:@"Проверьте подключение к интернету" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
        return;
    }
    
    [wView addWaitinViewToView:self.view];
    [rModel getCarWashesWithCompletion:^(BOOL success, NSArray *carWashs, NSString *error) {
        if ( success ) {
            [[UserObj sharedInstance] setCarWashes:carWashs];
            carWashes = carWashs;
            [self getFreeTimes];
        }
        else {
            [[[UIAlertView alloc] initWithTitle:@"Внимание" message:error delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
        }
        [wView removeWaitActivity];
    }];
}


- (void) getFreeTimes {
    if ( ![APP hasInternet] ) {
        [[[UIAlertView alloc] initWithTitle:@"Внимание" message:@"Проверьте подключение к интернету" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
        return;
    }
    
    NSDictionary *carW1 = carWashes[0];
    NSDictionary *carW2 = carWashes[1];
    [_rcwBtn setTitle:str(carWashes[0][@"name"]) forState:UIControlStateNormal];
    [_korkemBtn setTitle:str(carWashes[1][@"name"]) forState:UIControlStateNormal];
    
    
    [wView addWaitinViewToView:self.view];
    [rModel getFreeTimesInCarwashWithID:str([[carW1 objectForKey:@"_id"] objectForKey:@"$oid"]) completion:^(BOOL success, NSDictionary *freeTimes, NSString *error) {
        if ( success ) {
            freeTimes1 = freeTimes;
            [rModel getFreeTimesInCarwashWithID:str([[carW2 objectForKey:@"_id"] objectForKey:@"$oid"]) completion:^(BOOL success, NSDictionary *freTimes, NSString *error) {
                if ( success ) {
                    freeTimes2 = freTimes;
                    [self washChanged];
                }
                else {
                    [[[UIAlertView alloc] initWithTitle:@"Внимание" message:error delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
                }
                [wView removeWaitActivity];
            }];
        }
        else {
            [[[UIAlertView alloc] initWithTitle:@"Внимание" message:error delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
            [wView removeWaitActivity];
        }
    }];
}


- (void) initTimeChooserWithFreeTimes1 {
    NSLog(@"Free times 1  %@",freeTimes1);
    if ( timeChooser2 ) [timeChooser2 removeFromSuperview];
    
    if ( freeTimesArray1 == nil ) {
        freeTimesArray1 = [NSMutableArray new];
        for(int i=0;i<=23;i++){
            NSString *str1 = [NSString stringWithFormat:@"%@",[freeTimes1 objectForKey:[NSString stringWithFormat:@"%02d:00",i]]];
            [freeTimesArray1 addObject:[self labelWithText:[NSString stringWithFormat:@"%02d - 00",i] isFree:[str1 isEqual:@"1"]]];
            str1 = [NSString stringWithFormat:@"%@",[freeTimes1 objectForKey:[NSString stringWithFormat:@"%02d:30",i]]];
            [freeTimesArray1 addObject:[self labelWithText:[NSString stringWithFormat:@"%02d - 30",i] isFree:[str1 isEqual:@"1"]]];
            
        }
    }
    if ( !timeChooser1 ) {
        timeChooser1 = [[CategorySliderView alloc] initWithSliderHeight:_viewForTimeChooser.frame.size.height-20 andCategoryViews:freeTimesArray1 categorySelectionBlock:^(UIView *categoryView, NSInteger categoryIndex) {
            selectedTime1 = [((UILabel*)categoryView.subviews[0]).text stringByReplacingOccurrencesOfString:@"-" withString:@":"];
            selectedTime1 = [selectedTime1 stringByReplacingOccurrencesOfString:@" " withString:@""];
            //NSLog(@"cateogry selected at index %d", (int)categoryIndex);
        }];
    }
    [timeChooser1 moveY:10 duration:0.1 complation:nil];
    [_viewForTimeChooser addSubview:timeChooser1];
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:(NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond) fromDate:[NSDate date]];
    int ctime = (int)[components hour]*60 + (int)[components minute], differ = 1000000;
    int freeTimeIndex = -1;
    for(int i=23;i>=0;i--){
        NSString *str1 = [NSString stringWithFormat:@"%@",[freeTimes1 objectForKey:[NSString stringWithFormat:@"%02d:30",i]]];
        if ( [str1 isEqual:@"1"] && differ > ABS(ctime-((i*60)+30)) ) {
            differ = ABS(ctime-((i*60)+30));
            freeTimeIndex = i*2+1;
        }
        str1 = [NSString stringWithFormat:@"%@",[freeTimes1 objectForKey:[NSString stringWithFormat:@"%02d:00",i]]];
        if ( [str1 isEqual:@"1"] && differ > ABS(ctime-(i*60)) ) {
            differ = ABS(ctime-(i*60));
            freeTimeIndex = i*2;
        }
    }
    if ( freeTimeIndex != -1 ) {
        UIView *vv = [UIView new]; vv.tag = freeTimeIndex;
        UITapGestureRecognizer *pg= [[UITapGestureRecognizer alloc] initWithTarget:nil action:nil];
        [vv addGestureRecognizer:pg];
        [timeChooser1 categoryViewTapped:pg];
    }
}
- (void) initTimeChooserWithFreeTimes2 {
    NSLog(@"Free times 2  %@",freeTimes2);
    if ( timeChooser1 ) [timeChooser1 removeFromSuperview];
    
    if ( freeTimesArray2 == nil ) {
        freeTimesArray2 = [NSMutableArray new];
        for(int i=0;i<=23;i++){
            NSString *str1 = [NSString stringWithFormat:@"%@",[freeTimes2 objectForKey:[NSString stringWithFormat:@"%02d:00",i]]];
            [freeTimesArray2 addObject:[self labelWithText:[NSString stringWithFormat:@"%02d - 00",i] isFree:[str1 isEqual:@"1"]]];
            str1 = [NSString stringWithFormat:@"%@",[freeTimes2 objectForKey:[NSString stringWithFormat:@"%02d:30",i]]];
            [freeTimesArray2 addObject:[self labelWithText:[NSString stringWithFormat:@"%02d - 30",i] isFree:[str1 isEqual:@"1"]]];
        }
    }
    if ( !timeChooser2 ) {
        timeChooser2 = [[CategorySliderView alloc] initWithSliderHeight:_viewForTimeChooser.frame.size.height-20 andCategoryViews:freeTimesArray2 categorySelectionBlock:^(UIView *categoryView, NSInteger categoryIndex) {
            selectedTime2 = [((UILabel*)categoryView.subviews[0]).text stringByReplacingOccurrencesOfString:@"-" withString:@":"];
            selectedTime2 = [selectedTime2 stringByReplacingOccurrencesOfString:@" " withString:@""];
            //NSLog(@"cateogry selected at index %d", (int)categoryIndex);
        }];
    }
    [timeChooser2 moveY:10 duration:0.1 complation:nil];
    [_viewForTimeChooser addSubview:timeChooser2];
    
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:(NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond) fromDate:[NSDate date]];
    int ctime = (int)[components hour]*60 + (int)[components minute], differ = 1000000;
    int freeTimeIndex = -1;;
    for(int i=0;i<=23;i++){
        NSString *str1 = [NSString stringWithFormat:@"%@",[freeTimes2 objectForKey:[NSString stringWithFormat:@"%02d:30",i]]];
        if ( [str1 isEqual:@"1"] && differ > ABS(ctime-((i*60)+30)) ) {
            differ = ABS(ctime-((i*60)+30));
            freeTimeIndex = i*2+1;
        }
        str1 = [NSString stringWithFormat:@"%@",[freeTimes2 objectForKey:[NSString stringWithFormat:@"%02d:00",i]]];
        if ( [str1 isEqual:@"1"] && differ > ABS(ctime-(i*60)) ) {
            differ = ABS(ctime-(i*60));
            freeTimeIndex = i*2;
        }
    }
    if ( freeTimeIndex != -1 ) {
        UIView *vv = [UIView new]; vv.tag = freeTimeIndex;
        UITapGestureRecognizer *pg= [[UITapGestureRecognizer alloc] initWithTarget:nil action:nil];
        [vv addGestureRecognizer:pg];
        [timeChooser2 categoryViewTapped:pg];
    }
}

- (UIView *)labelWithText:(NSString *)text isFree:(BOOL)isFree {
    float w = Scn_Width/2.2;
    UIImageView *imgV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, w, _viewForTimeChooser.frame.size.height-20)];
    imgV.image = [UIImage imageNamed:@"wheel_item.png"];
    
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, w, _viewForTimeChooser.frame.size.height-20)];
    [lbl setFont:[UIFont systemFontOfSize:15]];
    [lbl setText:text];
    if ( isFree ) lbl.textColor = RGBA(0, 232, 89, 1);
    else lbl.textColor = RGBA(255, 48, 48, 1);
    
    [lbl setTextAlignment:NSTextAlignmentCenter];
    [imgV addSubview:lbl];
    
    return imgV;
}



- (void) customize {
    _enrollBtn.layer.cornerRadius = 5;
    _priceLabel.text = strFormat(@"%d тг", _sumPrice);
    _rcwBtn.layer.cornerRadius = 5;
    _korkemBtn.layer.cornerRadius = 5;
    _rcwBtn.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    _rcwBtn.titleLabel.textAlignment = NSTextAlignmentCenter; // if you want to
    
    if ( [[UIScreen mainScreen] bounds].size.height < 568 ) {
        _bottomOffsetConstraint.constant = 5;
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)washChanged {
    if ( _rcwBtn.tag == 1 ) [self initTimeChooserWithFreeTimes1];
    else [self initTimeChooserWithFreeTimes2];
    
    // Creates a marker in the center of the map.
    if(!marker)marker = [[GMSMarker alloc] init];
    
    NSDictionary *carW = carWashes[_korkemBtn.tag];
    
    marker.position = CLLocationCoordinate2DMake([carW[@"lat"] doubleValue], [carW[@"lon"] doubleValue]);
    marker.title = str(carW[@"name"]);
    marker.map = mapView_;
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[carW[@"lat"] doubleValue]
                                                            longitude:[carW[@"lon"] doubleValue]
                                                                 zoom:15];
    [mapView_ animateToCameraPosition:camera];
    [mapView_ setSelectedMarker:marker];
}



- (IBAction)goBackBtnTouched:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)washBtnTouched:(id)sender {
    UIButton *btn = (UIButton*)sender;
    if ( btn.tag == 1) return;
    _rcwBtn.tag = 0;
    _korkemBtn.tag = 0;
    [_rcwBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_rcwBtn setBackgroundColor:RGBA(23, 23, 23, 1)];
    [_korkemBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_korkemBtn setBackgroundColor:RGBA(23, 23, 23, 1)];
    
    btn.tag = 1;
    [btn setTitleColor:RGBA(26, 201, 85, 1) forState:UIControlStateNormal];
    [btn setBackgroundColor:RGBA(54, 54, 54, 1)];
    
    [self washChanged];
}
- (IBAction)enrollBtnTouched:(id)sender {
    if ( ![APP hasInternet] ) {
        [[[UIAlertView alloc] initWithTitle:@"Внимание" message:@"Проверьте подключение к интернету" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
        return;
    }
    
    [wView addWaitinViewToView:self.view];
   
    NSDictionary *carW = carWashes[_korkemBtn.tag];
    NSString *carwashID = [[carW objectForKey:@"_id"] objectForKey:@"$oid"];
    NSString *time;
    if ( _rcwBtn.tag == 1 ) time = selectedTime1;
    else                    time = selectedTime2;
    
    [rModel doOrderWithCarwashID:carwashID time:time auto_tip:_asID service_types:_serviceTypes online_order:@"" completion:^(BOOL success, NSDictionary *orde, NSString *error) {
        if ( success ) {
            //[[UserObj sharedInstance] setCurrentOrder:orde];
            [[UserObj sharedInstance] setFromSuccessEnroll:1];
            [self.navigationController popToRootViewControllerAnimated:NO];
            [[[UIAlertView alloc] initWithTitle:@"Благодарим за запись!" message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
        }
        else {
            [[[UIAlertView alloc] initWithTitle:@"Внимание" message:error delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
        }
        [wView removeWaitActivity];
    }];
}

- (IBAction)menuBtnTouched:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:@"Отмена"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Выйти из аккаунта", nil];
    [actionSheet showInView:self.view];
}
#pragma mark - Action sheet delegate
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ( buttonIndex == 0 ) {
        [[UserObj sharedInstance] setInfo:[NSDictionary new]];
        //[[UserObj sharedInstance] setCurrentOrder:nil];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}


@end
