//
//  ContactsViewController.m
//  Rolyal Car Wash
//
//  Created by Yerassyl on 11.09.15.
//  Copyright (c) 2015 acelight. All rights reserved.
//

#import "ContactsViewController.h"
#import "RoyalModel.h"
#import "WaitingView.h"
#import "UserObj.h"
#import "Defs.h"
#import "AppDelegate.h"

@interface ContactsViewController ()
@property (strong, nonatomic) IBOutlet UIButton *rcwBtn;
@property (strong, nonatomic) IBOutlet UIButton *korkemBtn;
@property (strong, nonatomic) IBOutlet UITextView *descTxtView;
@property (strong, nonatomic) IBOutlet UILabel *addressLabel;
@property (strong, nonatomic) IBOutlet UILabel *phoneLabel;

@end

@implementation ContactsViewController {
    NSArray *carWashes;
    
    RoyalModel *rModel;
    WaitingView *wView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    rModel = [RoyalModel new];
    wView = [WaitingView new];
    
    [self customize];
    if ( ![[UserObj sharedInstance] carWashes] ) [self getCarWashes];
    else {
        carWashes = [[UserObj sharedInstance] carWashes];
        [self washChanged];
        [_rcwBtn setTitle:str(carWashes[0][@"name"]) forState:UIControlStateNormal];
        [_korkemBtn setTitle:str(carWashes[1][@"name"]) forState:UIControlStateNormal];
    }
}

- (void) customize {
    _rcwBtn.layer.cornerRadius = 5;
    _korkemBtn.layer.cornerRadius = 5;
    _rcwBtn.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    _rcwBtn.titleLabel.textAlignment = NSTextAlignmentCenter; // if you want to
}



- (void) getCarWashes {
    if ( ![APP hasInternet] ) {
        [[[UIAlertView alloc] initWithTitle:@"Внимание" message:@"Проверьте подключение к интернету" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
        return;
    }
    
    [wView addWaitinViewToView:self.view];
    [rModel getCarWashesWithCompletion:^(BOOL success, NSArray *carWashs, NSString *error) {
        if ( success ) {
            [[UserObj sharedInstance] setCarWashes:carWashs];
            carWashes = carWashs;
            [_rcwBtn setTitle:str(carWashs[0][@"name"]) forState:UIControlStateNormal];
            [_korkemBtn setTitle:str(carWashs[1][@"name"]) forState:UIControlStateNormal];
            [self washChanged];
        }
        else {
            [[[UIAlertView alloc] initWithTitle:@"Внимание" message:error delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
        }
        [wView removeWaitActivity];
    }];
}


- (void)washChanged {
    NSDictionary *dic = carWashes[_korkemBtn.tag];
    _addressLabel.text = str(dic[@"address"]);
    _phoneLabel.text = strFormat(@"Тел:%@", dic[@"phone1"]);
    _descTxtView.text = [NSString stringWithFormat:@"%@\n\n\n%@%@\n%@%@",str(dic[@"description"]), @"e-mail: ",str(dic[@"email"]), @"Тел1: ",str(dic[@"phone1"])];
    if ( ![str(dic[@"phone2"]) isEqualToString:@""] )
        _descTxtView.text = [NSString stringWithFormat:@"%@\n%@%@",_descTxtView.text,@"Тел2: ",str(dic[@"phone2"])];
        if ( ![str(dic[@"phone3"]) isEqualToString:@""] )
            _descTxtView.text = [NSString stringWithFormat:@"%@\n%@%@",_descTxtView.text,@"Тел3: ",str(dic[@"phone3"])];
    
    [self.view layoutIfNeeded];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backBtnTouched:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)washBtnTouched:(id)sender {
    UIButton *btn = (UIButton*)sender;
    if ( btn.tag == 1) return;
    _rcwBtn.tag = 0;
    _korkemBtn.tag = 0;
    [_rcwBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_rcwBtn setBackgroundColor:RGBA(23, 23, 23, 1)];
    [_korkemBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_korkemBtn setBackgroundColor:RGBA(23, 23, 23, 1)];
    
    btn.tag = 1;
    [btn setTitleColor:RGBA(26, 201, 85, 1) forState:UIControlStateNormal];
    [btn setBackgroundColor:RGBA(54, 54, 54, 1)];
    
    [self washChanged];
}

@end
