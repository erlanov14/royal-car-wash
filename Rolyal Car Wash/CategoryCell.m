//
//  CategoryCell.m
//  Rolyal Car Wash
//
//  Created by Yerassyl on 12.09.15.
//  Copyright (c) 2015 acelight. All rights reserved.
//

#import "CategoryCell.h"
#import "Defs.h"

@implementation CategoryCell {
    NSString *imgStr;
}

- (void)awakeFromNib {
    self.layer.masksToBounds = NO;
    self.layer.shadowColor = [UIColor blackColor].CGColor;
    self.layer.shadowOffset = CGSizeMake(10, 10);
    self.layer.shadowRadius = 4;
    self.layer.shadowOpacity = 1;
    self.layer.cornerRadius = 2;
    self.layer.borderWidth = 0.5;
    self.layer.borderColor = RGBA(48, 47, 47, 1).CGColor;
    self.backgroundColor = RGBA(255, 255, 255, 0.08);
    
    [self.imgView setContentMode:UIViewContentModeScaleAspectFit];
}

- (void)initWithTitle:(NSString*)title img:(NSString*)img {
    _titleLbl.text = title;
    _imgView.image = [UIImage imageNamed:img];
    imgStr = img;
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    if (highlighted) {
        _imgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@_active.png",[imgStr stringByReplacingOccurrencesOfString:@".png" withString:@""]]];
        self.backgroundColor = RGBA(32, 174, 245, 0.5);
    } else {
        self.backgroundColor = RGBA(255, 255, 255, 0.08);
        _imgView.image = [UIImage imageNamed:imgStr];
    }
}

@end
