//
//  RoyalModel.m
//  Rolyal Car Wash
//
//  Created by Yerassyl on 11.09.15.
//  Copyright (c) 2015 acelight. All rights reserved.
//

#import "RoyalModel.h"
#import <AFNetworking.h>
#import "UserObj.h"
#import "Defs.h"


@implementation RoyalModel

- (void)registerUserWithName:(NSString *)name phone:(NSString *)phone carNumber:(NSString *)carNumber status:(NSString *)status completion:(void (^)(BOOL,NSString*))completionBlock {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *parameters = @{ @"user[phone]":phone, @"user[name]":name, @"user[car_number]":carNumber, @"user[status]":status };
    [manager POST:[NSString stringWithFormat:@"%@mobile_users/registration",mainURL] parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *dd = (NSDictionary*)responseObject;
        if ( ![dd objectForKey:@"error"] ) {
            if ( completionBlock ) { completionBlock(YES,nil); }
            [[UserObj sharedInstance] setInfo:dd];
        }
        else {
            if ( completionBlock ) { completionBlock(NO,str([dd objectForKey:@"error"])); }
        }
        //NSLog(@"JSON: %@", responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        if ( completionBlock ) {
            completionBlock(NO,@"Ошибка системы. Попробуйте позже");
        }
    }];
}
- (void)authorizeWithPhone:(NSString *)phone completion:(void (^)(BOOL, NSString *))completionBlock {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *parameters = @{ @"user[phone]":phone };
    [manager POST:[NSString stringWithFormat:@"%@mobile_users/login",mainURL] parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *dd = (NSDictionary*)responseObject;
        if ( ![dd objectForKey:@"error"] ) {
            if ( completionBlock ) { completionBlock(YES,nil); }
            [[UserObj sharedInstance] setInfo:dd];
        }
        else {
            if ( completionBlock ) { completionBlock(NO,str([dd objectForKey:@"error"])); }
        }
        //NSLog(@"JSON: %@", responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        if ( completionBlock ) {
            completionBlock(NO,@"Ошибка системы. Попробуйте позже");
        }
    }];
}


- (void)getPromotionsWithCompletion:(void (^)(BOOL, NSArray *, NSString *))completionBlock {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:[NSString stringWithFormat:@"%@posts",mainURL] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSArray *dd = (NSArray*)responseObject;
        if ( completionBlock ) { completionBlock(YES,dd,nil); }
        //NSLog(@"JSON: %@", responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        if ( completionBlock ) {
            completionBlock(NO,nil,@"Ошибка системы. Попробуйте позже");
        }
    }];
}

- (void)getAutoServicesWithCompletion:(void (^)(BOOL, NSArray *, NSString *))completionBlock {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:[NSString stringWithFormat:@"%@auto_services",mainURL] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSArray *dd = (NSArray*)responseObject;
        if ( completionBlock ) { completionBlock(YES,dd,nil); }
        //NSLog(@"JSON: %@", responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        if ( completionBlock ) {
            completionBlock(NO,nil,@"Ошибка системы. Попробуйте позже");
        }
    }];
}

- (void)getAutoServiceWithASID:(NSString *)asID completion:(void (^)(BOOL, NSDictionary *, NSString *))completionBlock {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:[NSString stringWithFormat:@"%@auto_services/%@",mainURL,asID] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *dd = (NSDictionary*)responseObject;
        if ( completionBlock ) { completionBlock(YES,dd,nil); }
        //NSLog(@"JSON: %@", responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        if ( completionBlock ) {
            completionBlock(NO,nil,@"Ошибка системы. Попробуйте позже");
        }
    }];
}

- (void)getFreeTimesInCarwashWithID:(NSString *)asID completion:(void (^)(BOOL, NSDictionary *, NSString *))completionBlock {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:[NSString stringWithFormat:@"%@boxes/free/time/%@",mainURL,asID] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *dd = (NSDictionary*)responseObject;
        if ( completionBlock ) { completionBlock(YES,dd,nil); }
        //NSLog(@"JSON: %@", responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        if ( completionBlock ) {
            completionBlock(NO,nil,@"Ошибка системы. Попробуйте позже");
        }
    }];
}
- (void)getCarWashesWithCompletion:(void (^)(BOOL, NSArray *, NSString *))completionBlock {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:[NSString stringWithFormat:@"%@car_washes",mainURL] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSArray *dd = (NSArray*)responseObject;
        if ( completionBlock ) { completionBlock(YES,dd,nil); }
        //NSLog(@"JSON: %@", responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        if ( completionBlock ) {
            completionBlock(NO,nil,@"Ошибка системы. Попробуйте позже");
        }
    }];
    //NSLog(@"keldi naa");
    //[self getOrdersCount];
}
- (void)doOrderWithCarwashID:(NSString *)carwashID time:(NSString *)time auto_tip:(NSString *)auto_tip service_types:(NSString *)service_types online_order:(NSString *)online_order completion:(void (^)(BOOL, NSDictionary *, NSString *))completionBlock {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSString *userPhone = str([[UserObj sharedInstance] info][@"phone"]);
    NSDictionary *parameters = @{ @"order[carwash_id]":carwashID, @"order[time]":time, @"order[user_phone]":userPhone, @"order[auto_tip]":auto_tip, @"order[service_types]":service_types, @"order[online_order]":@"true"};
    
    NSLog(@"params %@",parameters);
    
    [manager POST:[NSString stringWithFormat:@"%@orders/order",mainURL] parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *dd = (NSDictionary*)responseObject;
        if ( ![dd objectForKey:@"error"] ) {
            if ( completionBlock ) { completionBlock(YES,dd,nil); }
        }
        else {
            if ( completionBlock ) { completionBlock(NO,nil,str([dd objectForKey:@"error"])); }
        }
        //NSLog(@"JSON: %@", responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        if ( completionBlock ) {
            completionBlock(NO,nil,@"Ошибка системы. Попробуйте позже");
        }
    }];
}
- (void)destroyOrderWithBoxID:(NSString *)boxID orderID:(NSString *)orderID completion:(void (^)(BOOL, NSString *))completionBlock {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSDictionary *parameters = @{ @"box_id":boxID, @"order_id":orderID};
    NSLog(@"params %@",parameters);
    
    [manager POST:[NSString stringWithFormat:@"%@orders/delete",mainURL] parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *dd = (NSDictionary*)responseObject;
        if ( ![dd objectForKey:@"error"] ) {
            if ( completionBlock ) { completionBlock(YES,nil); }
        }
        else {
            if ( completionBlock ) { completionBlock(NO,str([dd objectForKey:@"error"])); }
        }
        //NSLog(@"JSON: %@", responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        if ( completionBlock ) {
            completionBlock(NO,@"Ошибка системы. Попробуйте позже");
        }
    }];
}

- (void)getOrdersCount {
    [[UserObj sharedInstance] setOrdersCount:0];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:[NSString stringWithFormat:@"%@orders/car_washes/all",mainURL] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *dd = (NSDictionary*)responseObject;
        [[UserObj sharedInstance] setOrdersCount:[dd[@"orders"] intValue]];
        //NSLog(@"JSON: %@", responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

- (void)getLastOrderWithCompletion:(void (^)(BOOL,NSDictionary*, NSString *))completionBlock {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:[NSString stringWithFormat:@"%@orders/user/%@/today",mainURL,str([[[UserObj sharedInstance] info] objectForKey:@"phone"])] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *dd = (NSDictionary*)responseObject;

       if  ( [responseObject isKindOfClass:[NSArray class]] ) {
           if ( ((NSArray*)responseObject).count > 0 ) {
               dd = ((NSArray*)responseObject)[0];
               //NSLog(@"orderres %@",dd);
               completionBlock(YES,dd,nil);
           }
           else {
               completionBlock(NO,nil,@"Вы не забронировали очередь");
           }
        }
        else {
            completionBlock(NO,nil,@"Вы не забронировали очередь");
        }
        //NSLog(@"JSON: %@", responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        completionBlock(NO,nil,@"Ошибка системы. Попробуйте позже");
    }];
}


- (void)sendDeviceToken {
    //NSLog(@"adding dev %@",[[UserObj sharedInstance] deviceToken]);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *parameters = @{ @"device_name":@"ios", @"phone" : str([[[UserObj sharedInstance] info] objectForKey:@"phone"]), @"token":[[UserObj sharedInstance] deviceToken]};
    [manager POST:[NSString stringWithFormat:@"%@tokens/add_token",mainURL] parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *dd = (NSDictionary*)responseObject;
        if ( ![dd objectForKey:@"error"] ) {
            
        }
        else {
        
        }
        //NSLog(@"JSON: %@", responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
    }];
}


@end
