//
//  BonusCell.h
//  Rolyal Car Wash
//
//  Created by Yerassyl on 11.09.15.
//  Copyright (c) 2015 acelight. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BonusCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgView;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;

- (void)initWithTitle:(NSString *)title img:(NSString *)img;

@end
