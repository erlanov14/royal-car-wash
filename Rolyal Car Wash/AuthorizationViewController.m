//
//  AuthorizationViewController.m
//  Rolyal Car Wash
//
//  Created by Yerassyl on 11.09.15.
//  Copyright (c) 2015 acelight. All rights reserved.
//

#import "AuthorizationViewController.h"
#import "LoginViewController.h"
#import "MainViewController.h"
#import "AFViewShaker.h"
#import "RoyalModel.h"
#import "UserObj.h"
#import "WaitingView.h"
#import "Defs.h"
#import "AppDelegate.h"


@interface AuthorizationViewController ()
@property (strong, nonatomic) IBOutlet UITextField *phoneTxtField;
@property (strong, nonatomic) IBOutlet UIButton *logInBtn;
@property (strong, nonatomic) IBOutlet UIButton *regBtn;

@end

@implementation AuthorizationViewController {
    RoyalModel *rModel;
    WaitingView *wView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    rModel = [RoyalModel new];
    wView = [WaitingView new];
    
    [self customize];
}

- (void)viewDidAppear:(BOOL)animated {
    NSString *phone = [[UserObj sharedInstance] info][@"phone"];
    if ( phone ) {
        MainViewController *mvc = [self.storyboard instantiateViewControllerWithIdentifier:@"MainViewController"];
        UINavigationController *nvc = [[UINavigationController alloc] initWithRootViewController:mvc];
        [nvc setNavigationBarHidden:YES];
        [self presentViewController:nvc animated:YES completion:nil];
    }
}

- (void) customize {
    _logInBtn.layer.cornerRadius = 5;
    _regBtn.layer.cornerRadius = 5;
    
    UIView *myV = [[UIView alloc] initWithFrame:CGRectMake(0, _phoneTxtField.frame.size.height-1, _phoneTxtField.frame.size.width, 1)];
    myV.backgroundColor = RGBA(224, 224, 224, 1);
    [_phoneTxtField addSubview:myV];
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, _phoneTxtField.frame.size.height)];
    _phoneTxtField.leftView = paddingView;
    _phoneTxtField.leftViewMode = UITextFieldViewModeAlways;
    NSAttributedString *placeholder = [[NSAttributedString alloc] initWithString:_phoneTxtField.placeholder attributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor] }];
    _phoneTxtField.attributedPlaceholder = placeholder;
    
    UIImageView *myI = [[UIImageView alloc] initWithFrame:CGRectMake(3, 0, _phoneTxtField.frame.size.height/2, _phoneTxtField.frame.size.height-8)];
    myI.image = [UIImage imageNamed:@"white_phone.png"];
    [myI setContentMode:UIViewContentModeScaleAspectFit];
    [_phoneTxtField addSubview:myI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)logInBtnTouched:(id)sender {
    if ( ![APP hasInternet] ) {
        [[[UIAlertView alloc] initWithTitle:@"Внимание" message:@"Проверьте подключение к интернету" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
        return;
    }
    
    
    [_phoneTxtField resignFirstResponder];
    if ( [_phoneTxtField.text isEqual:@""] ) {
        [[[AFViewShaker alloc] initWithView:_phoneTxtField]  shakeWithDuration:0.6 completion:^{}];
        return;
    }
    [wView addWaitinViewToView:self.view];
    [rModel authorizeWithPhone:_phoneTxtField.text completion:^(BOOL success, NSString *error) {
        if ( success ) {
            MainViewController *mvc = [self.storyboard instantiateViewControllerWithIdentifier:@"MainViewController"];
            UINavigationController *nvc = [[UINavigationController alloc] initWithRootViewController:mvc];
            [nvc setNavigationBarHidden:YES];
            [self presentViewController:nvc animated:YES completion:nil];
        }
        else {
            [[[UIAlertView alloc] initWithTitle:@"Внимание" message:error delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
        }
        [wView removeWaitActivity];
    }];

}
- (IBAction)regBtnTouched:(id)sender {
    [_phoneTxtField resignFirstResponder];
    LoginViewController *lvc = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
    [self presentViewController:lvc animated:YES completion:nil];
}


@end
