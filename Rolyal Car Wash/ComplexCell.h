//
//  ComplexCell.h
//  Rolyal Car Wash
//
//  Created by Yerassyl on 13.09.15.
//  Copyright (c) 2015 acelight. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ComplexCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UILabel *titleLbl;
@property (strong, nonatomic) IBOutlet UILabel *priceLbl;
@property (strong, nonatomic) IBOutlet UIImageView *checkBoxImgView;
@property (strong, nonatomic) IBOutlet UIImageView *imgView;

- (void)initWithTitle:(NSString*)title price:(NSString*)price img:(NSString*)img;

@end
