//
//  CategoryCell.h
//  Rolyal Car Wash
//
//  Created by Yerassyl on 12.09.15.
//  Copyright (c) 2015 acelight. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgView;
@property (strong, nonatomic) IBOutlet UILabel *titleLbl;

- (void)initWithTitle:(NSString*)title img:(NSString*)img;

@end
