//
//  MainViewController.m
//  Rolyal Car Wash
//
//  Created by Yerassyl on 11.09.15.
//  Copyright (c) 2015 acelight. All rights reserved.
//

#import "MainViewController.h"
#import "ContactsViewController.h"
#import "GeoViewController.h"
#import "BonusesViewController.h"
#import "LeftTimeViewController.h"
#import "CategoriesViewController.h"
#import "PriceViewController.h"
#import "UserObj.h"
#import "MainCell.h"
#import "RoyalModel.h"
#import "Defs.h"

@interface MainViewController () <UITableViewDataSource,UITableViewDelegate,UIActionSheetDelegate>
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation MainViewController {
    NSArray *titles, *imgs;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[RoyalModel new] getOrdersCount];
    [[RoyalModel new] sendDeviceToken];
    
    titles = @[@"Забронировать очередь",@"Осталось времени",@"Бонусы и акции",@"Прейскурант цен на автомойку",@"Контакты",@"Геокординаты автомойки"];
    imgs = @[@"order.png",@"left_time.png",@"bonuses.png",@"prices.png",@"phone.png",@"geo.png"];
    
    [self initTable];
    _nameLabel.text = str([[UserObj sharedInstance] info][@"name"]);
}

- (void)viewDidAppear:(BOOL)animated {
    if([[UserObj sharedInstance] fromSuccessEnroll] == 1) {
        LeftTimeViewController *bvc = [self.storyboard instantiateViewControllerWithIdentifier:@"LeftTimeViewController"];
        [self.navigationController pushViewController:bvc animated:YES];
    }
    if([[UserObj sharedInstance] fromSuccessPrice] == 1) {
        PriceViewController *pvc = [self.storyboard instantiateViewControllerWithIdentifier:@"PriceViewController"];
        [self.navigationController pushViewController:pvc animated:YES];
    }
}


- (IBAction)userBtnTouched:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                               delegate:self
                                      cancelButtonTitle:@"Отмена"
                                 destructiveButtonTitle:nil
                                      otherButtonTitles:@"Выйти из аккаунта", nil];
    [actionSheet showInView:self.view];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Action sheet delegate
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ( buttonIndex == 0 ) {
        [[UserObj sharedInstance] setInfo:[NSDictionary new]];
        //[[UserObj sharedInstance] setCurrentOrder:nil];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}


#pragma mark - table view delegate
- (void)initTable {
    _tableView.backgroundColor = [UIColor clearColor];
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    
    [_tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 6;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 15;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"MainCell";
    
    MainCell *cell = (MainCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[MainCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell initWithTitle:titles[indexPath.section] img:imgs[indexPath.section]];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if ( indexPath.section == 0 ) {
        CategoriesViewController *bvc = [self.storyboard instantiateViewControllerWithIdentifier:@"CategoriesViewController"];
        [self.navigationController pushViewController:bvc animated:YES];
    } else if ( indexPath.section == 1 ) {
        LeftTimeViewController *bvc = [self.storyboard instantiateViewControllerWithIdentifier:@"LeftTimeViewController"];
        [self.navigationController pushViewController:bvc animated:YES];
    } else if ( indexPath.section == 2 ) {
        BonusesViewController *bvc = [self.storyboard instantiateViewControllerWithIdentifier:@"BonusesViewController"];
        [self.navigationController pushViewController:bvc animated:YES];
    } else if ( indexPath.section == 3 ) {
        CategoriesViewController *bvc = [self.storyboard instantiateViewControllerWithIdentifier:@"CategoriesViewController"];
        bvc.isPricecurant = 1;
        [self.navigationController pushViewController:bvc animated:YES];
    } else if ( indexPath.section == 4 ) {
        ContactsViewController *gvc = [self.storyboard instantiateViewControllerWithIdentifier:@"ContactsViewController"];
        [self.navigationController pushViewController:gvc animated:YES];
        
    } else if ( indexPath.section == 5 ) {
        GeoViewController *gvc = [self.storyboard instantiateViewControllerWithIdentifier:@"GeoViewController"];
        [self.navigationController pushViewController:gvc animated:YES];
    }
}



@end
