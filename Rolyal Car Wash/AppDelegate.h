//
//  AppDelegate.h
//  Rolyal Car Wash
//
//  Created by Yerassyl on 11.09.15.
//  Copyright (c) 2015 acelight. All rights reserved.
//

#import <UIKit/UIKit.h>
@import GoogleMaps;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property BOOL hasInternet;

@end

