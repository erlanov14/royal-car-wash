//
//  GeoViewController.m
//  Rolyal Car Wash
//
//  Created by Yerassyl on 11.09.15.
//  Copyright (c) 2015 acelight. All rights reserved.
//

#import "GeoViewController.h"
#import "RoyalModel.h"
#import "WaitingView.h"
#import "UserObj.h"
#import "Defs.h"
#import "AppDelegate.h"
@import GoogleMaps;


#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)


@interface GeoViewController ()
@property (strong, nonatomic) IBOutlet UIButton *rcwBtn;
@property (strong, nonatomic) IBOutlet UIButton *korkemBtn;
@property (strong, nonatomic) IBOutlet UIView *mapContainerView;

@end


@implementation GeoViewController {
    GMSMarker *marker;
    CLLocationManager *locationManager;
    
    NSArray *carWashes;
    
    RoyalModel *rModel;
    WaitingView *wView;
    GMSMapView *mapView_;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   
    
    rModel = [RoyalModel new];
    wView = [WaitingView new];
    
    [self initGoogleMaps];
    [self customize];
    if ( ![[UserObj sharedInstance] carWashes] ) [self getCarWashes];
    else {
        carWashes = [[UserObj sharedInstance] carWashes];
        [self washChanged];
        [_rcwBtn setTitle:str(carWashes[0][@"name"]) forState:UIControlStateNormal];
        [_korkemBtn setTitle:str(carWashes[1][@"name"]) forState:UIControlStateNormal];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    mapView_.frame = CGRectMake(0, 0, _mapContainerView.frame.size.width, _mapContainerView.frame.size.height);
}

- (void)initGoogleMaps {
    locationManager = [[CLLocationManager alloc] init];
#ifdef IS_OS_8_OR_LATER
    if(IS_OS_8_OR_LATER) {
        [locationManager requestWhenInUseAuthorization];
        [locationManager requestAlwaysAuthorization];
    }
#endif
    [locationManager startUpdatingLocation];
    
    mapView_ = [GMSMapView mapWithFrame:CGRectZero camera:nil];
    mapView_.myLocationEnabled = YES;
    mapView_.frame = CGRectMake(0, 0, _mapContainerView.frame.size.width, _mapContainerView.frame.size.height);
    [_mapContainerView addSubview:mapView_];
}

- (void) getCarWashes {
    if ( ![APP hasInternet] ) {
        [[[UIAlertView alloc] initWithTitle:@"Внимание" message:@"Проверьте подключение к интернету" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
        return;
    }
    
    [wView addWaitinViewToView:self.view];
    [rModel getCarWashesWithCompletion:^(BOOL success, NSArray *carWashs, NSString *error) {
        if ( success ) {
            [[UserObj sharedInstance] setCarWashes:carWashs];
            carWashes = carWashs;
            [_rcwBtn setTitle:str(carWashs[0][@"name"]) forState:UIControlStateNormal];
            [_korkemBtn setTitle:str(carWashs[1][@"name"]) forState:UIControlStateNormal];
            [self washChanged];
        }
        else {
            [[[UIAlertView alloc] initWithTitle:@"Внимание" message:error delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
        }
        [wView removeWaitActivity];
    }];
}


- (void) customize {
    _rcwBtn.layer.cornerRadius = 5;
    _korkemBtn.layer.cornerRadius = 5;
    _rcwBtn.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    _rcwBtn.titleLabel.textAlignment = NSTextAlignmentCenter; // if you want to
}

- (void)washChanged {
    // Creates a marker in the center of the map.
    if(!marker)marker = [[GMSMarker alloc] init];
    
    NSDictionary *carW = carWashes[_korkemBtn.tag];
    
    marker.position = CLLocationCoordinate2DMake([carW[@"lat"] doubleValue], [carW[@"lon"] doubleValue]);
    marker.title = str(carW[@"name"]);
    marker.map = mapView_;
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[carW[@"lat"] doubleValue]
                                                            longitude:[carW[@"lon"] doubleValue]
                                                                 zoom:15];
    [mapView_ animateToCameraPosition:camera];
    [mapView_ setSelectedMarker:marker];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backBtnTouched:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)washBtnTouched:(id)sender {
    UIButton *btn = (UIButton*)sender;
    if ( btn.tag == 1) return;
    _rcwBtn.tag = 0;
    _korkemBtn.tag = 0;
    [_rcwBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_rcwBtn setBackgroundColor:RGBA(23, 23, 23, 1)];
    [_korkemBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_korkemBtn setBackgroundColor:RGBA(23, 23, 23, 1)];
    
    btn.tag = 1;
    [btn setTitleColor:RGBA(26, 201, 85, 1) forState:UIControlStateNormal];
    [btn setBackgroundColor:RGBA(54, 54, 54, 1)];
    
    [self washChanged];
}

@end
