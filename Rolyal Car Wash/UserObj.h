//
//  UserObj.h
//  Rolyal Car Wash
//
//  Created by Yerassyl on 11.09.15.
//  Copyright (c) 2015 acelight. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserObj : NSObject

@property (nonatomic) NSDictionary *info;
@property (nonatomic) NSString *deviceToken;
@property (nonatomic) NSArray *carWashes;

@property (nonatomic) NSString *curantPrice;

@property int fromSuccessEnroll, fromSuccessPrice;

@property (nonatomic) int ordersCount;


+ (id)sharedInstance;

- (NSDictionary*)getInfo;
- (void)setInfo:(NSDictionary *)info;

@end
