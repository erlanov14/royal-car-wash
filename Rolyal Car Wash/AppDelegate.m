//
//  AppDelegate.m
//  Rolyal Car Wash
//
//  Created by Yerassyl on 11.09.15.
//  Copyright (c) 2015 acelight. All rights reserved.
//

#import "AppDelegate.h"
#import "KSReachability.h"
#import "UserObj.h"

@interface AppDelegate ()
@property(strong,nonatomic) KSReachability* internetReachability;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [UIApplication sharedApplication].statusBarHidden = YES;
    [GMSServices provideAPIKey:@"AIzaSyBS7MR8TlzXJ0jdouJXOp2ZLjLHvKzEEiM"];
    
    // push notification settings
    {
        // Регистируем девайс на приём push-уведомлений
        if ([application respondsToSelector:@selector(isRegisteredForRemoteNotifications)])
        {
            // iOS 8 Notifications
            [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
            
            [application registerForRemoteNotifications];
        }
        else
        {
            // iOS < 8 Notifications
            [application registerForRemoteNotificationTypes:
             (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound)];
        }

    }
    
    
    // reachability for internet connection checking
    {
        // Create a new reachability object.
        _hasInternet = YES;
        self.internetReachability = [KSReachability reachabilityToInternet];
        self.internetReachability.onInitializationComplete = ^(KSReachability *reachability)
        {
            //NSLog(@"Internet reachability initialization complete. Reachability = %d. Flags = %x", reachability.reachable, reachability.flags);
        };
        // Set a callback.
        self.internetReachability.onReachabilityChanged = ^(KSReachability* reachability)
        {
            NSLog(@"Internet reachability changed to %d. Flags = %x (blocks)", reachability.reachable, reachability.flags);
            _hasInternet = reachability.reachable;
        };
    }
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


#pragma mark - push notification
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken  {
    NSLog(@"My token is: %@", deviceToken);
    [[UserObj sharedInstance]setDeviceToken:[NSString stringWithFormat:@"%@",deviceToken]];
}
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"Error: %@", error.description);
}
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
 
}


@end
