//
//  BonusViewController.m
//  Rolyal Car Wash
//
//  Created by Yerassyl on 11.09.15.
//  Copyright (c) 2015 acelight. All rights reserved.
//

#import "BonusViewController.h"
#import "UIImageView+WebCache.h"

@interface BonusViewController ()
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIImageView *imgView;
@property (strong, nonatomic) IBOutlet UITextView *descTxtView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *titleHeightConstraint;

@end

@implementation BonusViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initBonus];
}

- (void) initBonus {
    _titleLabel.text = [_bonus objectForKey:@"title"];
    _descTxtView.text = [_bonus objectForKey:@"description"];
    [_imgView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://royalcarwash.kz/%@",[_bonus objectForKey:@"banner"]]]];
    
    NSLog(@"URL %@",[NSString stringWithFormat:@"http://royalcarwash.kz/%@",[_bonus objectForKey:@"banner"]]);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)goBackBtnTouched:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
