//
//  BonusesViewController.m
//  Rolyal Car Wash
//
//  Created by Yerassyl on 11.09.15.
//  Copyright (c) 2015 acelight. All rights reserved.
//

#import "BonusesViewController.h"
#import "BonusViewController.h"
#import "Defs.h"
#import "BonusCell.h"
#import "RoyalModel.h"
#import "WaitingView.h"
#import "Defs.h"
#import "AppDelegate.h"

@interface BonusesViewController () <UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation BonusesViewController {
    NSArray *promotions;
    RoyalModel *rModel;
    WaitingView *wView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    rModel = [RoyalModel new];
    wView = [WaitingView new];
    
    
    [self initTable];
    [self getPromotions];
}
- (void)viewDidAppear:(BOOL)animated {
    
}

- (void) getPromotions{
    if ( ![APP hasInternet] ) {
        [[[UIAlertView alloc] initWithTitle:@"Внимание" message:@"Проверьте подключение к интернету" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
        return;
    }
    
    [wView addWaitinViewToView:self.view];
    [rModel getPromotionsWithCompletion:^(BOOL success, NSArray *promotios, NSString *error) {
        if ( success ) {
            self->promotions = promotios;
            [_tableView reloadData];
        }
        else {
            [[[UIAlertView alloc] initWithTitle:@"Внимание" message:error delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
        }
        [wView removeWaitActivity];
    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backBtnTouched:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - table view delegate
- (void)initTable {
    _tableView.backgroundColor = [UIColor clearColor];
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    
    [_tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return promotions.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 15;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"BonusCell";
    
    BonusCell *cell = (BonusCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[BonusCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    NSDictionary *dic = promotions[indexPath.section];
    [cell initWithTitle:[dic objectForKey:@"title"] img:[NSString stringWithFormat:@"http://royalcarwash.kz/%@",[dic objectForKey:@"banner"]]];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    BonusViewController *bvc=[self.storyboard instantiateViewControllerWithIdentifier:@"BonusViewController"];
    bvc.bonus = promotions[indexPath.section];
    [self.navigationController pushViewController:bvc animated:YES];
}





@end
