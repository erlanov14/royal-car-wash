//
//  CategoriesViewController.m
//  Rolyal Car Wash
//
//  Created by Yerassyl on 12.09.15.
//  Copyright (c) 2015 acelight. All rights reserved.
//

#import "CategoriesViewController.h"
#import "ComplexesViewController.h"
#import "RoyalModel.h"
#import "WaitingView.h"
#import "AFViewShaker.h"
#import "CategoryCell.h"
#import "UserObj.h"
#import "Defs.h"
#import "AppDelegate.h"


#define kNumberOfCellsInARow 2

@interface CategoriesViewController () <UICollectionViewDataSource,UICollectionViewDelegate,UIActionSheetDelegate>
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) IBOutlet UILabel *onLineLabel;
@property (strong, nonatomic) IBOutlet UIButton *okBtn;
@property (strong, nonatomic) IBOutlet UILabel *chooseCatLabel;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;

@end

@implementation CategoriesViewController {
    NSArray *imgs, *asIds;
    NSMutableArray *titles;
    int isChosen[10];
    
    RoyalModel *rModel;
    WaitingView *wView;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    rModel = [RoyalModel new];
    wView = [WaitingView new];
    
    for(int i=0;i<10;i++)isChosen[i] = 0;
    asIds = @[@"55cdbf82726f790875000008",@"55cdbf82726f790875000006",@"55cdbf82726f790875000007",  @"55cdbf82726f790875000005", @"55cdbf82726f790875000009", @"55cdbf82726f790875000004" ];
    titles = [NSMutableArray arrayWithArray:@[@"Микро Автобус",          @"Седаны",                  @"Фургоны",                   @"Мини кроссоверы",          @"Микро Автобус COASTER",    @"Внедорожник"]];
    imgs = @[@"microavtobus.png",@"sedan.png",@"furgon.png",@"mini_crossover.png",@"coaster.png",@"suv.png"];
    
    
    [self initCollection];
    [self updateCollectionViewLayout];
    [self customize];
    [self getAutoServices];
    
    _onLineLabel.text = [NSString stringWithFormat:@"%d",[[UserObj sharedInstance] ordersCount]];
    _nameLabel.text = str([[UserObj sharedInstance] info][@"name"]);
}

- (void) getAutoServices {
    if ( ![APP hasInternet] ) {
        [[[UIAlertView alloc] initWithTitle:@"Внимание" message:@"Проверьте подключение к интернету" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
        return;
    }
    
    [wView addWaitinViewToView:self.view];
    [rModel getAutoServicesWithCompletion:^(BOOL success, NSArray *autoService, NSString *error) {
        if ( success ) {
            for(NSDictionary * dd in autoService) {
                NSString *oid =  str([[dd objectForKey:@"_id"] objectForKey:@"$oid"]);
                for(int i=0;i<asIds.count;i++){
                    if ( [asIds[i] isEqualToString:oid] ) {
                        titles[i] = str([dd objectForKey:@"name"]);
                        [_collectionView reloadData];
                    }
                }
            }
        }
        else {
            [[[UIAlertView alloc] initWithTitle:@"Внимание" message:error delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
        }
        [wView removeWaitActivity];
    }];

}


- (void) customize {
    _okBtn.layer.cornerRadius = 5;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backBtnTouched:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)okBtnTouched:(id)sender {
    int chosemInd = -1;
    for(int i=0;i<6;i++){
        if ( isChosen[i] == 1 ) chosemInd = i;
    }
    if ( chosemInd == -1 ) {
        [[[AFViewShaker alloc] initWithView:_chooseCatLabel]  shakeWithDuration:0.6 completion:^{}];
        return;
    }
    ComplexesViewController *cvc = [self.storyboard instantiateViewControllerWithIdentifier:@"ComplexesViewController"];
    cvc.asID = asIds[chosemInd];
    cvc.asImg = imgs[chosemInd];
    cvc.isPricecurant = _isPricecurant;
    [self.navigationController pushViewController:cvc animated:YES];
}


- (IBAction)menuBtnTouched:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:@"Отмена"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Выйти из аккаунта", nil];
    [actionSheet showInView:self.view];
}
#pragma mark - Action sheet delegate
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ( buttonIndex == 0 ) {
        [[UserObj sharedInstance] setInfo:[NSDictionary new]];
        //[[UserObj sharedInstance] setCurrentOrder:nil];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}


#pragma mark - collection view delegate
- (void)initCollection {
    ((UIScrollView*)(_collectionView)).delegate = self;
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    
    
    _collectionView.backgroundColor = [UIColor clearColor];
    self.collectionView.alwaysBounceVertical = YES;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 6;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 4.0;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 8.0;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CategoryCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CategoryCell" forIndexPath:indexPath];
    if ( isChosen[indexPath.row] == 0 ) [cell initWithTitle:titles[indexPath.row] img:imgs[indexPath.row]];
    else [cell initWithTitle:titles[indexPath.row] img:[NSString stringWithFormat:@"%@_active.png",[imgs[indexPath.row] stringByReplacingOccurrencesOfString:@".png" withString:@""]]];
    
    if ( isChosen[indexPath.row] == 1 ) cell.backgroundColor = RGBA(32, 174, 245, 0.5);
    else                cell.backgroundColor = RGBA(255, 255, 255, 0.08);
    
    
    return cell;
}
- (void)updateCollectionViewLayout
{
    UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout;
    CGFloat size = (Scn_Width-22) / kNumberOfCellsInARow;
    if ( IS_IPHONE_6P ) size -= 3;
    
    //NSLog(@"%f",size);
    layout.itemSize = CGSizeMake(size, size*0.647);
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath  {
    CategoryCell *datasetCell = (CategoryCell*)[collectionView cellForItemAtIndexPath:indexPath];
    if ( datasetCell.selected ) {
        datasetCell.imgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@_active.png",[imgs[indexPath.row] stringByReplacingOccurrencesOfString:@".png" withString:@""]]];
        datasetCell.backgroundColor = RGBA(32, 174, 245, 0.5);
        isChosen[indexPath.row] = 1;
    }
}
- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    CategoryCell *datasetCell = (CategoryCell*)[collectionView cellForItemAtIndexPath:indexPath];
    if ( !datasetCell.selected ) {
        datasetCell.backgroundColor = RGBA(255, 255, 255, 0.08);
        datasetCell.imgView.image = [UIImage imageNamed:imgs[indexPath.row]];
        isChosen[indexPath.row] = 0;
    }
}




@end
