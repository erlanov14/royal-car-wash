//
//  MainCell.m
//  Rolyal Car Wash
//
//  Created by Yerassyl on 11.09.15.
//  Copyright (c) 2015 acelight. All rights reserved.
//

#import "MainCell.h"
#import "Defs.h"

@implementation MainCell

- (void)awakeFromNib {
    // Initialization code
    self.layer.masksToBounds = NO;
    self.layer.shadowColor = [UIColor blackColor].CGColor;
    self.layer.shadowOffset = CGSizeMake(10, 10);
    self.layer.shadowRadius = 4;
    self.layer.shadowOpacity = 1;
    self.layer.cornerRadius = 2;
    self.layer.borderWidth = 0.5;
    self.layer.borderColor = RGBA(48, 47, 47, 1).CGColor;
    self.backgroundColor = RGBA(255, 255, 255, 0.08);
    
    [self.imgView setContentMode:UIViewContentModeScaleAspectFit];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)initWithTitle:(NSString *)title img:(NSString *)img {
    _titleLabel.text = title;
    _imgView.image = [UIImage imageNamed:img];
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    if (highlighted) {
        self.backgroundColor = RGBA(32, 174, 245, 0.5);
    } else {
        self.backgroundColor = RGBA(255, 255, 255, 0.08);
    }
}

@end
