//
//  WaitingView.m
//  Rolyal Car Wash
//
//  Created by Yerassyl on 11.09.15.
//  Copyright (c) 2015 acelight. All rights reserved.
//

#import "WaitingView.h"

@implementation WaitingView  {
    UIView *trView;
    UIActivityIndicatorView *act;
    NSMutableURLRequest *request;
}
- (void)addWaitinViewToView:(UIView *)view {
    if ( trView == nil ) trView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, view.frame.size.width, view.frame.size.height)];
    if ( act == nil )    act = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    act.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    [act setColor:[UIColor whiteColor]];
    [trView setBackgroundColor:[UIColor colorWithWhite:0 alpha:.1]];
    [view addSubview:trView];
    [view addSubview:act];
    //dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
    //    dispatch_async(dispatch_get_main_queue(), ^{
            [act setCenter:view.center];
    //    });
    //});
    [act startAnimating];
}
- (void) removeWaitActivity {
    [act removeFromSuperview];
    [trView removeFromSuperview];
    [act stopAnimating];
}


@end
