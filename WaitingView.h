//
//  WaitingView.h
//  Rolyal Car Wash
//
//  Created by Yerassyl on 11.09.15.
//  Copyright (c) 2015 acelight. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface WaitingView : NSObject 

- (void) addWaitinViewToView:(UIView*)view;
- (void) removeWaitActivity ;

@end
